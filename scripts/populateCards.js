const fs = require("fs");
const { parse } = require("csv-parse");
const { currentVersion, mercenaries: mercs, compare } = require('../src/gameConfig.js');

const potentialMercenaries = [];
process.argv.forEach(function (val, index, array) {

  const getMerc = (v) => {
    switch (v) {
      case 'f': return 'foulborn';
      case 'c': return 'cassius';
      case 'd': return 'dreya';
      case 's': return 'scourge';
      default: return v;
    }
  }

  if (index > 1) {
      potentialMercenaries.push(getMerc(val))
  }
});

const mercenaries = potentialMercenaries.length > 0 ? potentialMercenaries : mercs;
// const compare = ["Dreya_Backups", "Scourge_Backups","Cassius_Backups","Foulborn_Backups"];
const allAbilities = [];
const allCompare = [];

console.log('Generating cards for ', mercenaries)

const parseAllCards = () => {
  const promises = [];
  compare.forEach((m) => {
    promises.push(
      new Promise((res, rej) => {
        fs.createReadStream(`./${currentVersion}/Mercenaries/${m}.csv`)
          .pipe(parse({ delimiter: ",", from_line: 1, columns: true }))
          .on("data", function (card) {
            if (!!card.Class && !!card.Title) {
              allCompare.push(card);
            }
          })
          .on("end", res);
      })
    );
  });
  mercenaries.forEach((m) => {
    promises.push(
      new Promise((res, rej) => {
        fs.createReadStream(`./${currentVersion}/Mercenaries/${m}.csv`)
          .pipe(parse({ delimiter: ",", from_line: 1, columns: true }))
          .on("data", function (card) {
            if (!!card.Class && !!card.Title) {
              allAbilities.push(card);
            }
          })
          .on("end", res);
      })
    );
  });
  Promise.all(promises).then(() => {
    console.log("Finished parsing CSV data. Num of cards: ", allAbilities.length);
    createIndividualCardFiles(allAbilities)
    try {
      fs.unlinkSync('./src/api/localData.ts');
    } catch(e) {}
    fs.appendFileSync('./src/api/localData.ts', `export const localAbilities = ${JSON.stringify(allAbilities)};\nexport const localCompare = ${JSON.stringify(allCompare)}`)
  });

};

const createIndividualCardFiles = (cards) => {
    cards.forEach(card => {
        let fileData = ``
        Object.keys(card).forEach(key => {
            fileData = `${fileData}${key}\n${card[key]}\n\n\n`
        })
        const fileName = `./${currentVersion}/Cards/${card.id}.txt`;
        try {
          fs.unlinkSync(fileName);
        } catch(e) {}
        fs.appendFileSync(fileName, fileData)
    })
}

parseAllCards();
