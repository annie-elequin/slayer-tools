Title
Feral Fox


Class
Dreya


Level
2


Type
Card


Scale
0


Image
https://cdn0.iconfinder.com/data/icons/animal-attack-human/314/animal-attack-010-512.png


Quantity
1


Spark
1


Description
^rushOne^Rush 1 and Gain ^Flex^Flex 1 <br/><br/> ^AllRight^ This Round: set all Min & Max Range values to 1. Each time you play a card that deals ^d^Dmg to an Enemy, ^Move^0-2


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre16


