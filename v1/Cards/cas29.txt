Title
Trade Routes


Class
Cassius


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-kj58TKk/0/0464d7b0/O/download%20%2820%29.png


Quantity
1


Spark
2


Description
^Discard^Discard 1-2<br/>Choose another Player to ^Draw^Draw X equal to the amount you Discarded<br/><br/>Gain ^apOne^ for each Card they Drew


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas29


