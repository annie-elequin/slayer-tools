Title
Scatter!


Class
Cassius


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-cfRtmw8/0/ae77d469/O/download%20%2811%29.png


Quantity
1


Spark
1


Description
^Scramble^Scramble 2 with^Jump^Jump and^b3^<br/>If you now ^Scatter^Scatter 1 to an adjacent Enemy, you may<br/>^Loop^Loop this up to 2 more times


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas15


