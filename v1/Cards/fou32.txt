Title
Takedown


Class
Foulborn


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Foulborn/i-cXtCXVw/0/73476515/O/Foulborn_takedown.png


Quantity
1


Spark
1


Description
(^Range^1): If you have more ^b^ Block than the Target, <br/> ^Disarm^Disarm them and deal ^oX^ equal to their ^b^ Block


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou32


