Title
Go Ballistic


Class
Scourge


Level
3


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_goballistic/public


Quantity
1


Spark
3


Description
^rushX^Rush 0-3 OR ^Draw^Draw 0-3<br/>^Jump^Scatter 0-3 OR Lose 0-3 HP


Keep
FALSE


Trigger
Reaction


TriggerAction
If you just lost at least 3 HP, you MUST use this Reaction. <br/>Perform all 4 actions above using their Max Values<br/>Gain 2 ^Flex^


Print
NO


Public
YES


id
sco37


