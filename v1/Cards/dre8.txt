Title
Fleet Fox


Class
Dreya


Level
1


Type
Card


Scale
20


Image
https://thumbs.dreamstime.com/b/simple-running-fox-silhouette-design-vector-color-as-you-wish-127848189.jpg


Quantity
1


Spark
1


Description
^Move^Move 0-3 <br/><br/>If you Moved less than 3, choose any Player to ^rushX^**Rush X** equal to unused Movement


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
dre8


