Title
Flar Gun


Class
Scourge


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Scourge/i-9R7hdgv/0/9182ee54/O/download%20%2811%29.png


Quantity
1


Spark
1


Description
(^Lob^Lob 3): Select any space<br/>If there is a Unit, ^h3^ to them<br/><br/>Then ^Pull^Pull 1 to ALL Units within<br/>(^Range^1+^spX^)


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
sco6


