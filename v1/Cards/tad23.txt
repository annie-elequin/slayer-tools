Title
Spring Trap


Class
Tad


Level
2


Type
Card


Scale
0


Image
https://cdn-icons-png.flaticon.com/512/1167/1167670.png


Quantity
2


Spark
1


Description
(R3) Choose any **Unit**. Force them to ^Jump^ **2** in the direction they are facing.


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad23


