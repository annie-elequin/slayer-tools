Title
Claw Swipe


Class
Foulborn


Level
0


Type
Card


Scale
35


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Foulborn/i-Fd7VTT5/0/fa56bb4c/O/vectorcarrot200700228%20copy.png


Quantity
2


Spark
1


Description
(^Range^**2**): ^d2^ <br/>You may Lose ^b2^ to Force the Target 0-1 spaces in any direction


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
fou27


