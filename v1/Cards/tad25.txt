Title
(Bot) Sentient Sawblade


Class
Tad


Level
3


Type
Power


Scale
0


Image
https://cdn4.iconfinder.com/data/icons/smile-rounded-1/512/xxx001-512.png


Quantity
3


Spark



Description
(Summon) **5** Block, 0 HP. You may summon this onto any unoccupied space within (R5, Line).^Tuck^ **1** to ^Keep^.


Keep
FALSE


Trigger



TriggerAction
Jump this Bot **X** Spaces in a Line and ^dX^ to all enemies adjacent to (or under) this bot's movement. If this **Stack**has 8 or more Spark, immediately ^Cast^ this Stack.


Print
NO


Public
NO


id
tad25


