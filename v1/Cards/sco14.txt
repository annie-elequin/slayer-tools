Title
Stomp


Class
Scourge


Level
1


Type
Card


Scale
0


Image
https://www.svgrepo.com/show/320443/boot-stomp.svg


Quantity
1


Spark
2


Description
^Jump^**Jump 0-1**, then (^Range^**1**):^d2^<br/><br/>On ^Hit^**Hit**, you may ^Loop^ **Loop** this card until you have Targeted any **Enemy** twice with this card


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
sco14


