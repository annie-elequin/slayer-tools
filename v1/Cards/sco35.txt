Title
Meltdown


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_meltdown/public


Quantity
1


Spark
1


Description
Select Exactly 3 Units within (^Range^3), including yourself, to gain ^b3^<br/>^spTwo^= Do this Again<br/>


Keep
FALSE


Trigger
Reaction


TriggerAction
(^Range^3,^Target^3, ^Pierce^Pierce): ^dX^ equal the ^b^Block of the Target witht the least Block


Print
NO


Public
YES


id
sco35


