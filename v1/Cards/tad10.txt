Title
Remote Control


Class
Tad


Level
1


Type
Card


Scale
0


Image
https://static.thenounproject.com/png/68497-200.png


Quantity
1


Spark
1


Description
^Cast^ **1**. Pick (1+^spX^) ****Allies**** within (R2). ^Move^ them **X** spaces equal to half the height of that Stack.


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad10


