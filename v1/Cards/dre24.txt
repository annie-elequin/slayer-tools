Title
Soaring Arrow


Class
Dreya


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-sc25vHj/0/ff57d565/O/arrow-hunting-weapon-icon.png


Quantity
1


Spark
1


Description
(^Lob^Lob 4/4): ^dX^ equal to your Distance from the Target <br/><br/>^spOne^= +2 Range<br/>^spTwo^= Double the total Range


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
dre24


