Title
Underhanded Tactics


Class
Cassius


Level
0


Type
Card


Scale
 0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/cassius_underhandedtactics/public


Quantity
1


Spark
0


Description
^b2^ and ^Draw^Draw 1


Keep
 FALSE


Trigger
Always


TriggerAction
If you ^Discard^Discard this from your hand,^TopDeck^Top-Deck it and ^b2^


Print
YES


Public
YES


id
cas42


