Title
Fire Alarm


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Scourge/i-9R7hdgv/0/9182ee54/O/download%20%2811%29.png


Quantity
1


Spark
1


Description
(^Lob^Lob 3): Select any space. If there is a Unit there, they ^h4^ and <br/>^Taunt^Taunt all adjacent Enemies.<br/><br/> If not, for all Enemies within (^Range^2) of the ^Target^Target, ^Pull^Pull 1 towards the ^Target^Target


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
sco39


