Title
Sadistic Strength


Class
Cassius


Level
2


Type
Card


Scale
40


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-zNPhMhb/0/3002473d/O/download%20%2821%29.png


Quantity
1


Spark
1


Description
^KeepTap^Keep = Lose 1 HP


Keep
TRUE


Trigger
Reaction


TriggerAction
If an adjacent Enemy is eliminated, you may Gain ^Flex^ equal to their total ^Wound^


Print
NO


Public
YES


id
cas37


