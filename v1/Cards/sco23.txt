Title
Ravager


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://previews.123rf.com/images/jemastock/jemastock2008/jemastock200802222/153243939-skull-head-with-bones-crossed-on-fire-silhouette-style-icon-vector-illustration-design.jpg


Quantity
1


Spark
1


Description
**Pull** (1+^spOne^) to <br/>any **Enemies** within ^Range^**4** <br/>For each **Enemy** now in ^Range^1,<br/>^Draw^Draw 1


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
sco23


