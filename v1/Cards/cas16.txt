Title
Lurk


Class
Cassius


Level
2


Type
Power


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-cFC2g87/0/db1ac7a3/O/download%20%2822%29.png


Quantity
1


Spark
---


Description



Keep
FALSE


Trigger
Reaction


TriggerAction
If an **Enemy** within ^Range^ **1** **Moves**, you may **Advance** ^Move^ towards them with equal movement and apply ^pX^ equal to the spaces you **Advanced**


Print
NO


Public
YES


id
cas16


