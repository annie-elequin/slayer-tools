Title
Secret Stab


Class
Cassius


Level
0


Type
Card


Scale
30


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-mCFtdtK/0/451847f6/O/Cassius_secretstab.png


Quantity
1


Spark
1


Description
(^Range^1):^d1^


Keep
FALSE


Trigger
Always


TriggerAction
If you^Discard^Discard this from your Hand, ^Reclaim^Reclaim it


Print
YES


Public
YES


id
cas9


