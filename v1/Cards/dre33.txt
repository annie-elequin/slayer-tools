Title
Canopy


Class
Dreya


Level
2


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/dreya_canopy/public


Quantity
1


Spark
1


Description
<img src="https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/dreya_canopyAOE/public"> Then Gain ^bX^ equal to Total ^bX^ gained by all other Units


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
dre33


