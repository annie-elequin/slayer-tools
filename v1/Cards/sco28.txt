Title
Thermal Charge


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://thumbs.dreamstime.com/b/black-silhouette-hand-grenade-army-explosive-weapon-icon-military-isolated-object-vector-illustration-virus-covid-178141871.jpg


Quantity
1


Spark
1


Description
<br/> <img src="https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_bombsawayAOE/public"> (!) All Units are Targeted by this attack<br/><br/>Choose a ^Line^ Line starting from you and ^Draw^Draw 1<br/><br/>^Lob^Lob this attack a distance equal to the ^Spark^Spark of the Card Drawn<br/>(On a 0, center it on yourself)<br/><br/>If you fail to ^Hit^Hit an Enemy, <br/>^Reclaim^Reclaim this Card


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
sco28


