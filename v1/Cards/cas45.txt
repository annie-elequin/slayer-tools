Title
Enraging Stab


Class
Cassius


Level
2


Type
Card


Scale
0


Image



Quantity
1


Spark
1


Description
(^Range^1):^d3^, ^Taunt^Taunt 2<br/><br/>If the Target will ^Seek^Target the nearest Unit, whether friendly or not


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas45


