Title
Simmer


Class
Scourge


Level
0


Type
Card


Scale
0


Image
https://cdn-icons-png.flaticon.com/512/1689/1689090.png


Quantity
1


Spark
1


Description
^Draw^Draw 1, then ^Discard^Discard 1<br/><br/>^rushX^Rush X equal to the ^Spark^Spark of the card you Discarded<br/><br/>^spOne^ = Also ^hX^ equal to<br/>double this value


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
sco11


