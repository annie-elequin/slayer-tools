Title
Toxic Claws


Class
Foulborn


Level
3


Type
Card


Scale
0


Image
https://media.istockphoto.com/vectors/scratch-claws-of-animal-tiger-claws-design-elementicon-logo-isolated-vector-id1187127008?k=20&m=1187127008&s=612x612&w=0&h=k1Eaym6xR6x9UZgU8wUq-Z4yj2LjoT15We0aCvSQCB4=


Quantity
1


Spark
1


Description
^AllRight^ this Round: Any time a card deals ^d^Dmg to a Unit,<br/> it also applies ^p1^Poison


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou26


