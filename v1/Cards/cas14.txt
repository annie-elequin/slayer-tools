Title
Dizzying Stab


Class
Cassius


Level
1


Type
Card


Scale
40


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-4dgzVQS/0/3a2835d0/O/download%20%287%29.png


Quantity
1


Spark
1


Description
(^Range^**1**): ^d2^ <br/>**Scatter**^Scatter^**1** to the **Target** 


Keep
FALSE


Trigger
Reaction


TriggerAction
^spOne^= Do the ability above


Print
YES


Public
YES


id
cas14


