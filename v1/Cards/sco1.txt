Title
Flare Up


Class
Scourge


Level
P


Type
Power


Scale
40


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Scourge/i-d5JkGD7/0/d55f9094/O/scourge_flareup.png


Quantity
1


Spark
---


Description
Lose ^spX^ **HP** <br/>Gain ^Flex^Flex X equal to this number


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
sco1


