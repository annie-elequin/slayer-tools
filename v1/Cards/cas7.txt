Title
In Plain Sight


Class
Cassius


Level
1


Type
Card


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-sgvw3fg/0/87204633/O/download%20%286%29.png


Quantity
1


Spark
1


Description
^b3^ for each adjacent Enemy.<br/><br/>If at least one of them is facing you,<br/>^rushTwo^Rush 2 OR ^b3^ 


Keep



Trigger



TriggerAction



Print
YES


Public
YES


id
cas7


