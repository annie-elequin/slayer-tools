Title
Pass The Torch


Class
Scourge


Level
2


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_passthetorch/public


Quantity
1


Spark
2


Description
Target any adjacent Unit with HP equal to or less than your Max HP<br/><br/>Swap HP with them<br/><br/>(The Target cannot have <br/>more than their Max HP)


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
sco36


