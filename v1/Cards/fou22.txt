Title
Clothesline


Class
Foulborn


Level
3


Type
Card


Scale
0


Image
https://cdn.pixabay.com/photo/2021/03/13/15/20/boxing-6091981_1280.png


Quantity
1


Spark
2


Description
(^Range^**2**): Select an **Enemy** <br/> Force them in one direction, keeping their distance from you, until they stop at an Obstacle or other Unit <br/> ^dX^ to the Target equal to the spaces they Moved <br/><br/>^spOne^= If they struck another Enemy, ^dX^ to them as well


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou22


