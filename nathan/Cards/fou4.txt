Title
Lurking Presence


Class
Foulborn


Level
0


Type
Card


Scale
30


Image
https://avatanplus.com/files/resources/original/5870fe8c625d215979625473.png


Quantity
1


Spark
1


Description
^Move^Move 0-3 <br/><br/> If you never entered a space adjacent to an Enemy, ^rushOne^Rush 1


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou4


