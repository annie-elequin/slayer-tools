Title
Pickpocket


Class
Cassius


Level
0


Type
Card


Scale
30


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-fF2bc3n/0/021c01c7/O/download%20%2838%29.png


Quantity
1


Spark
2


Description
^Discard^Discard 1-3 <br/><br/>(^Range^2, ^Target^3): ^dX^<br/> equal to Double the number of Cards Discarded. <br/><br/> ^Draw^Draw 1 for each Target


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas9


