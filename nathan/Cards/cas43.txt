Title
Noxious Concotion


Class
Cassius


Level
1


Type
Card


Scale
10


Image



Quantity
1


Spark
1


Description
Apply ^p3^ to adjacent Enemies OR ^p2^ to Enemies within (^Range^2)


Keep
FALSE


Trigger
Reaction


TriggerAction
If an adjacent Enemy would ^Seek^Target you, you may Remove ^p2^Poison from them in order to ^Hide^Hide instead


Print
YES


Public
YES


id
cas43


