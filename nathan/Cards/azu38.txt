Title
Critical Blast


Class
Azura


Level
3


Type
Card


Scale
50


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Azura/i-DQcxFZN/0/3bb30c67/O/icon_2549426_edited.png


Quantity
1


Spark
2


Description
(^Line^ **3/3**): ^d7^


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
azu38


