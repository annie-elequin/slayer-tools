Title
Armor Polish


Class
Foulborn


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Foulborn/i-fsqwdz5/0/79937bb6/O/Foulborn_armorpolish.png


Quantity
1


Spark
2


Description
^Draw^Draw (1+^spOne^)<br/> ^Tuck^Tuck 1 <br/> Gain X ^Armor^Armor equal to the ^Spark^Spark of the card Tucked


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou30


