Title
Contagion


Class
Dreya


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-MK5Ffdh/0/a3c6b3ad/O/dreya_contagion.png


Quantity
1


Spark
2


Description
(^Range^4): Select any Enemy <br/> Copy up to <br/>^p3^ OR 2 Debuffs<br/> onto its closest Ally <br/><br/>^spTwo^=Select the affected Ally and repeat this ability, targeting a new Enemy


Keep
FALSE


Trigger
spTwo


TriggerAction



Print
NO


Public
YES


id
dre23


