Title
Adaptation


Class
Foulborn


Level
3


Type
Card


Scale
15


Image
https://image.shutterstock.com/image-vector/vector-flat-illustration-black-silhouette-260nw-737456401.jpg


Quantity
1


Spark



Description
**Cure** (2+^spOne^), **Targeting** yourself <br/> ^spOne^ = ^Heal^**Heal 2** for each **Cure**


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou21


