Title
Shoulder Bash


Class
Foulborn


Level
1


Type
Card


Scale
25


Image
https://www.pngrepo.com/png/323325/180/spiked-shoulder-armor.png


Quantity
1


Spark
1


Description
^Move^Move 1-2 in a Line</br>(^Range^**1**): ^d2^, ^Push^**Push 0-1** <br/><br/>You may Lose 2 ^b^ to ^Loop^**Loop** this card if you continue moving in the same direction


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou13


