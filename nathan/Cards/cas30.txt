Title
Back Stab


Class
Cassius


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-mPHCJRJ/0/409ff60a/O/download%20%2813%29.png


Quantity
1


Spark
2


Description
(^Range^1): ^d2^ <br/> If you are behind the Target, choose<br/>^Pierce^Pierce OR Double the effect of ^Wound^Wound


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas30


