Title
Territorial


Class
Foulborn


Level
1


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Foulborn/i-rhqXmcq/0/2e9bedc2/O/Territorial.png


Quantity
1


Spark
1


Description
Gain ^Flex^Flex and ^rushOne^Rush 1 for each adjacent Obstacle <br/><br/>^spOne^=Destroy an adjacent Obstacle, ^Draw^Draw 2, and ^Taunt^Taunt all Enemies adjacent to the Obstacle


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou8


