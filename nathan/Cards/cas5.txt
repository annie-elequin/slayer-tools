Title
Easy Pickings


Class
Cassius


Level
0


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-647mRzc/0/d4ddd546/O/download%20%2830%29.png


Quantity
1


Spark
0


Description
(^Range^2): ^Wound^Wound 1


Keep
FALSE


Trigger
Reaction


TriggerAction
If you succesfully ^Hide^Hide, <br/>you may ^Reclaim^Reclaim this card to your Hand


Print
NO


Public
YES


id
cas5


