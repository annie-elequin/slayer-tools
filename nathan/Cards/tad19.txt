Title
Grappling Hook


Class
Tad


Level
2


Type
Card


Scale
0


Image
https://cdn-icons-png.flaticon.com/512/3604/3604841.png


Quantity
2


Spark
0


Description
^Line^ **5** Select a **Obstacle** or Unit in range <br/> ^Move^ in a Line until you are adjacent to it <br/>[*] = ^4^ to the **Target**


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad19


