Title
Sock-It Wrench


Class
Tad


Level
1


Type
Card


Scale
0


Image
https://cdn.w600.comps.canstockphoto.com/socket-wrench-set-outline-vector-illustration_csp19901524.jpg


Quantity
2


Spark
1


Description
(R1) Steal 0-2 ^b^, then ^d1^. If the **Target** is an **Ally**, then give them ^b3^ instead.


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad14


