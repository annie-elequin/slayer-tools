Title
Lookout Duty


Class
Cassius


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-Rm6Zdpw/0/dedd9472/O/download%20%2819%29.png


Quantity
1


Spark
2


Description
Spy **3**. <br/>Each time you **Discard** a card in this way, **Scramble 1**


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas20


