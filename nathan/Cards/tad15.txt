Title
Tad's Trusty Taser


Class
Tad


Level
1


Type
Card


Scale
0


Image
https://cdn1.vectorstock.com/i/1000x1000/04/85/taser-icon-in-black-style-isolated-on-white-vector-13530485.jpg


Quantity
1


Spark
1


Description
^KeepTap^**Keep** = Give ^b1-2^ to any **Unit** within (R2), including yourself


Keep
TRUE


Trigger
Reaction


TriggerAction
(R2) Select an **Enemy** with Block equal to or double your own Block, then Stun them.


Print
NO


Public
NO


id
tad15


