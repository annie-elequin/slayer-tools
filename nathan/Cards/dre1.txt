Title
Boundless Quiver


Class
Dreya


Level
P


Type
Power


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-bKmfTc3/0/e75cdf45/O/download%20%2816%29.png


Quantity
1


Spark
---


Description



Keep
FALSE


Trigger
Always


TriggerAction
Every time you play an ^Arrow^Arrow card and deal ^d^ Damage to an Enemy, you may <br/>^waitOne^Wait 1 to ^Draw^Draw 1


Print
YES


Public
YES


id
dre1


