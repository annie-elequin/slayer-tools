Title
Measure The Breeze


Class
Azura


Level
3


Type
Power


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Azura/i-nSk9g7t/0/d550a154/O/Measure%20The%20Breeze.png


Quantity
1


Spark
---


Description
Your ^Line^ attacks may 'bend' one space off their path one space (if they have enough Range), stopping immediately


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
azu18


