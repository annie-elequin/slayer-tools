Title
Toxin-Tipped Blades


Class
Cassius


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-ghcv4Br/0/e58c6046/O/download%20%2817%29.png


Quantity
1


Spark
1


Description
^AllRight^This Round: each time you deal ^d^Damage to an Enemy, also apply ^p1^ to them


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas31


