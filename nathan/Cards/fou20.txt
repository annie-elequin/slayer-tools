Title
Burrowing Bruiser


Class
Foulborn


Level
3


Type
Power


Scale
0


Image
https://cdn3.iconfinder.com/data/icons/man-digging-hole/480/dig-hole-1-512.png


Quantity
1


Spark
---


Description
^spOne^ = You may pass through Traps and Enemies freely until your next turn. <br/><br/> ^spTwo^ = In addition to above, each time you pass through an Enemy they gain ^Pin^**Pin** and **Trap Damage**


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
fou20


