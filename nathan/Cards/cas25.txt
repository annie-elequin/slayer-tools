Title
Shade Cloak


Class
Cassius


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-XP48CZ3/0/e1116363/O/download%20%2831%29.png


Quantity
1


Spark
0


Description
^AllRight^While in play: each time an Enemy would target you with a ^Seek^Seek, force them to ^Seek^ again and lose 2 HP


Keep
FALSE


Trigger
Reaction


TriggerAction
If an Ally within ^Range^3 would be targeted by a ^Seek^, you may perform this same action and ^Discard^Discard this


Print
NO


Public
YES


id
cas25


