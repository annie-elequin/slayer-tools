Title
Precise Slug


Class
Azura


Level
3


Type
Card


Scale
25


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Azura/i-vNqVjsc/0/0a35cb57/O/High-Caliber%20Mark.png


Quantity
1


Spark
2


Description
^Line^ **5** ^dX^ where **X** is equal to your distance from the **Target**


Keep
FALSE


Trigger
Fatal


TriggerAction
^Cast^ **1**


Print
NO


Public
NO


id
azu32


