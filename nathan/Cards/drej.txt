Title
Stalking Arrow


Class
Dreya


Level
2


Type
Card


Scale
0


Image
https://thumbs.dreamstime.com/b/black-bird-was-hit-arrow-silhouette-vector-isolated-white-background-black-bird-was-hit-arrow-silhouette-vector-184428837.jpg


Quantity
1


Spark
1


Description
^Move^Move 0-1 <br/>(^Lob^4/4): ^d3^ <br/><br/>If there were 0 Targetable Enemies in Range of this card's attack, you may ^Loop^Loop this card


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
drej


