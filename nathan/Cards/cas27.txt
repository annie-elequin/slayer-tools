Title
Soothsayer


Class
Cassius


Level
2


Type
Card


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-KZhPBHJ/0/6301dfbc/O/download%20%2836%29.png


Quantity
1


Spark
1


Description
For all enemies within (R3) with any ^Wound^, apply ^Weak^**Weak**


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
YES


id
cas27


