Title
Greased Wheels


Class
Tad


Level
1


Type
Card


Scale
0


Image
https://thumbs.dreamstime.com/b/cutout-silhouette-oiler-dripping-drop-oil-lubricating-two-spinning-gears-outline-icon-motor-oil-black-illustration-178163633.jpg


Quantity
1


Spark
2


Description
Choose any **Player**or **Ally** within (R2) to ^Move^ 0-2


Keep
FALSE


Trigger
Loop


TriggerAction
If you ^Tuck^ **1**, ^Loop^ this card.


Print
NO


Public
NO


id
tad6


