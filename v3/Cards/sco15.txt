Title
Hit The Gas


Class
Scourge


Order
8


Level



Scale
0


Image
https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTb4rTy_ETXiBBNrjcJYSFq4wSO8cRGkiAtSA8gkBV9n569qNHZ0V8wJgxd6eXtCF2EaMk&usqp=CAU


Quantity
1


Spark
X


Cost



Power
1


OnSlot



OnUse
^Draw^Draw until the number of cards in your Hand equals your ^Rage^Rage


Reaction



ReactionAction



Special
Always


SpecialAction
The Cost of this Card is equal to your current ^Rage^Rage


Description



Trigger



TriggerAction



Print
YES


Public
YES


id
sco15


Speed
0


Tactics
0






