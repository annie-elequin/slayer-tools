Title
Cannonball


Class
Scourge


Order
9


Level



Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_cannonball/public


Quantity
1


Spark
1


Cost



Power
2


OnSlot



OnUse
^Jump^Jump 2-3<br/>
<br/>Deal ^d1^ Damage to all adjacent Enemies
<br/>
<br/>Lose half of your HP (rounded down)
<br/>
<br/>^Disarm^Disarm one of your Targets for each 4 HP you lost in this way


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
sco13


Speed
0


Tactics
0






