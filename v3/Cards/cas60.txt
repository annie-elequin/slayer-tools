Title
Smuggle


Class
Cassius


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-2qxJqsB/0/f9a49620/O/Smuggle.png


Quantity
1


Spark
1


Cost
1


Power
2


OnSlot



OnUse
^Discard^Discard up to (1+^SparkX^) Cards, then <br/>^Draw^Draw that many


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas60


Speed
0


Tactics
0


