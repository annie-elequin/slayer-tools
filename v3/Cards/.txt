Title
Steadfast


Class
Foulborn


Level
1


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/foulborn_steadfastAOE2/quality


Quantity



Spark



Cost



OnSlot



OnUse
Gain ^bX^ Block equal to 4 times the number of Enemies targeted 
<br/>(by either the Block or Taunt)
<br/>then ^Taunt^Taunted enemies act 


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print



Public



id



Power



Speed



Tactics







