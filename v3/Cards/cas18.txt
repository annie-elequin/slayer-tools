Title
Chain Leash


Class
Cassius


Order
17


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-XthFVfL/0/781e66c4/O/chain%20leash.png


Quantity
1


Spark
1


Cost
2


Power
2


OnSlot



OnUse
(^Line^Line 3): Deal ^d2^ Damage
<br/>
<br/>Choose to ^Pull^Pull 1 to the Target or
<br/>^Pull^Pull yourself until you are adjacent to the Target


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas18


Speed
0


Tactics
0


