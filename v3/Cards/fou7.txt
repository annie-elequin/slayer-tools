Title
Stare Down


Class
Foulborn


Order
7


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-XgL33Wd/0/9ff96326/O/download%20%286%29.png


Quantity
1


Spark
1


Cost
TT


Power
1


OnSlot
(Range^Range^2): ^Pull^Pull 0-1 to an Enemy and turn them in any direction


OnUse
Gain ^bX^Block equal to <br/>(2+^SparkX^)
<br/>
<br/>Then repeat for each adjacent Enemy facing you


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou7


Speed
0


Tactics
0


