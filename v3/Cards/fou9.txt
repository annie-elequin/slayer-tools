Title
Shield Bash


Class
Foulborn


Order
5


Level
S


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-JXFN4NZ/0/3d640a9c/O/shield-bash.png


Quantity
1


Spark
2


Cost
ST


Power
1


OnSlot



OnUse
Gain ^b5^ Block<br/>^Move^Move 0-2<br/><img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-6GJZNhJ/0/e3f8ca46/O/Foulborn_ShieldBash%20%286%29.png" width="400" height="250"> <br/>Lose 1-4 ^b^Block and deal an equal amount of ^dX^ Damage


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou9


Speed
0


Tactics
0


