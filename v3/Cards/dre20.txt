Title
Poison Tip


Class
Dreya


Order
15


Level



Scale
0


Image
https://wiki.melvoridle.com/images/thumb/3/3d/Poison_Arrows_%28item%29.png/250px-Poison_Arrows_%28item%29.png


Quantity
1


Spark
1


Cost
1


Power
1


OnSlot
^Draw^Draw 1


OnUse
The ^Next^Next card you Use will swap its final ^d^ Damage values for ^p^ Poison values


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
dre20


Speed
0


Tactics
0






d
d


https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png
https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png


S
S


