Title
Charlatan Cloak


Class
Cassius


Order
32


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-SjdXG2q/0/c267a04e/O/Charlatan%20Cloak.png


Quantity
1


Spark
1


Cost
1


Power
0


OnSlot
Gain and Place ^Spark^^Spark^^Spark^ on this


OnUse



Reaction
If you receive ^d^ Damage


ReactionAction
Reduce the incoming ^d^Damage by 3 for each ^Spark^ still on this card


Special
While Slotted


SpecialAction
You may remove 1 ^Spark^ to ^Jump^Jump behind an adjacent Enemy


Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas52


Speed
0


Tactics
0


