Title
Easy Pickings


Class
Cassius


Order
6


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-whBH2WV/0/79173963/O/download%20%2830%29.png


Quantity
1


Spark
1


Cost
1


Power
2


OnSlot
^Jump^Jump 0-2 and end in a space adjacent to an
<br/>Enemy without ^Wound^Wound


OnUse
Apply (1+^SparkOne^) ^Wound^Wound to an adjacent Enemy


Reaction



ReactionAction



Special
If ^Reclaim^Reclaimed


SpecialAction
Remove one ^Wound^Wound from an adjacent Enemy to Slot this again


Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas5


Speed
0


Tactics
0


