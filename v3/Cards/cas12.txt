Title
Sidestep


Class
Cassius


Order
12


Level



Scale
30


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-CZkcf2n/0/05f775c5/O/Sidestep.png


Quantity
1


Spark
1


Cost
2


Power
1


OnSlot
Gain ^b3^


OnUse
^Move^Move 0-3, always entering spaces adjacent to a Unit of your choice


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas12


Speed
0


Tactics
0


