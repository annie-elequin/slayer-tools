Title
Screech


Class
Cassius


Order
10


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-TfmHNkw/0/9647522d/O/Screech.png


Quantity
1


Spark
1


Cost
1


Power
1


OnSlot



OnUse
^Disarm^Disarm all Enemies within 
<br/>(Range^Range^ 1), then ^Taunt^Taunt at least one of them
<br/>
<br/>^SparkOne^= Increase this Range by 1


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas58


Speed
0


Tactics
0


