Title
Contagion


Class
Dreya


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-MK5Ffdh/0/a3c6b3ad/O/dreya_contagion.png


Quantity
1


Spark
2


Cost
2


Power



OnSlot



OnUse
(Range^Range^4): Select any Enemy 
<br/>Copy up to 
<br/>^p3^ OR 2 Debuffs
<br/> onto its closest Ally 
<br/>
<br/>^spTwo^=Select the affected Ally and repeat this ability, targeting a new Enemy


Reaction
spTwo


ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
dre32


Speed
0


Tactics
0






d
d


https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png
https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png


S
S


