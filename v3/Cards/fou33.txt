Title
Steadfast


Class
Foulborn


Order
27


Level



Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/foulborn_steadfast/quality


Quantity
1


Spark
2


Cost
PT


Power
2


OnSlot



OnUse
<img src="https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/foulborn_steadfastAOE2/quality" width="400" height="400">
<br/> Gain ^bX^ Block equal to 4 times the number of Enemies targeted 
<br/>(by either the Block or Taunt)
<br/>then ^Taunt^Taunted enemies act 


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou33


Speed
0


Tactics
0


