Title
Shock Collar


Class
Tad


Level
3


Type
Card


Scale
0


Image
https://www.caninejournal.com/wp-content/uploads/shock-collar-icon-png.png


Quantity
3


Spark
1


Description
Gain **3** **Taunt**. Until the start of your next turn, ^p4^ to any **Enemy** each time they Hit you.[*] = They also receive **1** Weak.


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad27


