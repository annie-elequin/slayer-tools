Title
Scatter


Class
Cassius


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-cfRtmw8/0/ae77d469/O/download%20%2811%29.png


Quantity
1


Spark
2


Cost
2


Power
2


OnSlot



OnUse
^Jump^Jump 2 spaces exactly and <br/>Gain^b4^Block<br/><br/>Force an adjacent Enemy to<br/>^Scatter^Scatter 1


Reaction
Always


ReactionAction
Slot and Fip this when Used once. You may Clear this card to Use it again


Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas36


Speed
0


Tactics
0


