Title
Special Reserve


Class
Cassius


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-5qBCSzF/0/92aaac07/O/Special%20Reserve.png


Quantity
1


Spark
1


Cost
1


Power
2


OnSlot



OnUse
^Draw^Draw (^SparkX^) Cards<br/>Then Gain (4-X) Tokens of any type, where X is the number of Cards in your Draw Pile


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas64


Speed
0


Tactics
0


