Title
Forager


Class
Dreya


Order
X


Level



Scale
20


Image
https://cdn-icons-png.flaticon.com/512/621/621717.png


Quantity
1


Spark
1


Cost
T


Power
1


OnSlot



OnUse
Look at the top (1+^SparkX^) Cards in your Draw Pile <br/><br/>^Discard^Discard any of them and ^Topdeck^Topdeck the rest in any order (do not collect Tokens)<br/><br/>
^SparkOne^ =Slot the top card of your Discard Pile


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
dre11


Speed
0


Tactics
0






d
d


https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png
https://i0.wp.com/www.atlaslisboa.com/wp-content/uploads/2017/05/wolf-trap-icon-10.png


S
S


