Title
Counterfeit


Class
Cassius


Order
X


Level



Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-4FTsMcc/0/d5843b04/O/download%20%2812%29.png


Quantity
1


Spark
1


Cost
1


Power
1


OnSlot



OnUse
^Draw^Draw 2 Cards


Reaction
Always


ReactionAction
If you ^Discard^Discard this from your hand, swap it for the top card of your Discard pile


Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas23


Speed
0


Tactics
0


