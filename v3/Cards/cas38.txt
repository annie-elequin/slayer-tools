Title
Secret Plans


Class
Cassius


Order
X


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-zNvNnG9/0/60beacee/O/Secret%20Plan.png


Quantity
1


Spark
3


Cost
3


Power
3


OnSlot



OnUse
Tuck up to (1+^SparkX^) cards underneath this


Reaction
Always


ReactionAction
Round Start: Clear this card and ^Reclaim^Reclaim the tucked cards


Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas38


Speed
0


Tactics
0


