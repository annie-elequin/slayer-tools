Title
Cratermaker


Class
Scourge


Order
13


Level



Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_cratermaker/quality


Quantity
1


Spark
2


Cost



Power
1


OnSlot



OnUse
^JumpLine^Jump X in a Line the exact number of spaces equal to your 
<br/>^Rage^Rage
<br/>
<br/>Deal ^dX^ to all adjacent Enemies equal to the distance you traveled


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
sco18


Speed
0


Tactics
0






