Title
Secret Stab


Class
Cassius


Order
5


Level
S


Scale
30


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-mCFtdtK/0/451847f6/O/Cassius_secretstab.png


Quantity
1


Spark
0


Cost
1


Power
0


OnSlot



OnUse
(Range^Range^1): Deal ^d1^ Damage
<br/>
<br/>On ^Hit^Hit: Gain ^Spark^


Reaction



ReactionAction



Special
If ^Reclaim^Reclaimed


SpecialAction
^Wound^ Wound an adjacent Enemy


Description



Trigger



TriggerAction



Print
YES


Public
YES


id
cas4


Speed
0


Tactics
0


