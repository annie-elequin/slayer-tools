Title
Launch


Class
Scourge


Order
25


Level



Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_launch/public


Quantity
1


Spark
1


Cost



Power
1


OnSlot
^Pull^Pull 0-1 to any Ally, Enemy, or Platform within Range^Range^2


OnUse
Choose an adjacent Unit or Platform<br/>^Push^Push it up to (2+^SparkX^) spaces with ^Jump^Jump
<br/>
<br/>Deal ^d1^ Damage to all Enemies adjacent to the landing space


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
sco8


Speed
0


Tactics
0






