Title
Dizzying Stab


Class
Cassius


Order
X


Level



Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-4dgzVQS/0/3a2835d0/O/download%20%287%29.png


Quantity
1


Spark
2


Cost
2


Power
2


OnSlot



OnUse
Target up to (1+^SparkX^) Enemies within (Range^Range^2)
<br/>
<br/>Deal ^d2^Damage and
<br/>^Scatter^Scatter 1 to all Targets


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
NO


Public
NO


id
cas28


Speed
0


Tactics
0


