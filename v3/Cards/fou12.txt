Title
Beat My Chest


Class
Foulborn


Order
13


Level



Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-DTrR9c4/0/6938595e/O/download%20%2811%29.png


Quantity
1


Spark
1


Cost
T


Power
2


OnSlot
Gain up to ^b3^Block


OnUse
Gain ^b4^Block and ^Taunt^Taunt the closest Enemy
<br/>
<br/> ^SparkOne^ = Gain up to ^b6^ Block and ^Taunt^Taunt the closest Enemy aside from your first Target


Reaction



ReactionAction



Special
While Slotted


SpecialAction
You must Use this ability to clear it


Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou12


Speed
0


Tactics
0


