Title
Tail Lash


Class
Foulborn


Order
9


Level



Scale
0


Image
https://www.pngrepo.com/png/321442/180/spiked-tentacle.png


Quantity
1


Spark
2


Cost
P


Power
1


OnSlot



OnUse
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-7zsRrfc/0/8ea07348/O/Foulborn%20Tail%20Lash%20%281%29.png" width="400" height="289">
<br/> ^SparkOne^ = ^Wound^Wound all Targets 
<br/> ^SparkOne^ = Apply ^p3^ Poison to Targets


Reaction



ReactionAction



Special



SpecialAction



Description



Trigger



TriggerAction



Print
YES


Public
YES


id
fou14


Speed
0


Tactics
0


