import React from "react";
import { ReactSVG } from "react-svg";

export const getClassIconUrl = cl => `classIcons/${cl.toLowerCase()}.svg`

export default function ClassIcon({ className, color = "#000", size = 40 }) {
  return (
    <ReactSVG
      src={getClassIconUrl(className)}
      beforeInjection={(svg) => {
        svg.setAttribute("class", "bp_Icon");

        if (color) {
          svg.setAttribute("fill", color);
        }

        if (size) {
          svg.setAttribute("width", `${size}`);
          svg.setAttribute("height", `${size}`);
        }
      }}
    />
  );
}
