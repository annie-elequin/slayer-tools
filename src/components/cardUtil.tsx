/* eslint-disable no-useless-escape */
/* eslint-disable import/no-webpack-loader-syntax */
import { marked } from "marked";
const { default: boxHtml } = require("raw-loader!./CanvasCard/BoxHtml.html");
const {
  default: ifThenBoxHtml,
} = require("raw-loader!./CanvasCard/IfThenBoxHtml.html");
const {
  default: reactionBoxHtml,
} = require("raw-loader!./CanvasCard/ReactionBoxHtml.html");
const {
  default: valueIconHtml,
} = require("raw-loader!./CanvasCard/ValueIconHtml.html");
const { default: iconHtml } = require("raw-loader!./CanvasCard/IconHtml.html");
const {
  default: cardTitleHtml,
} = require("raw-loader!./CanvasCard/CardTitleHtml.html");

const emboldenKeywords = (original) => {
  const keywords = JSON.parse(localStorage.getItem("ifcKeywords") ?? "");
  let result: string = original;
  let foundWord = false;
  keywords.forEach((k) => {
    if (result.includes(k)) {
      let regexp = new RegExp(k, "g");
      let match;
      while ((match = regexp.exec(result)) !== null) {
        if (!!`${result.slice(match.index - 10, match.index)}${result.slice(regexp.lastIndex, Math.min(regexp.lastIndex + 5, result.length - 1))}`.match(/\s/g)) {
            // it's not in a link, we can use it
            foundWord = true;
            result = `${result.slice(0, match.index)}<strong>${k}</strong>${result.slice(regexp.lastIndex)}`
        }
      }
    }
  });
  return foundWord ? result : original;
};

export const CARD_WIDTH = 822;
export const CARD_HEIGHT = 1122;
export const DPI = 300;

export const sparkSize = CARD_WIDTH * 0.13;
export const sparkFontSize = CARD_WIDTH * 0.07;
export const sparkBorderWidth = CARD_WIDTH * 0.009;
export const sparkTopValue = CARD_HEIGHT * 0.015;
export const sparkLeftValue = CARD_HEIGHT * 0.018;

export const costTopValue = CARD_HEIGHT * 0.07;
export const costLeftValue = CARD_HEIGHT * 0.02;
export const costIconSize = (str = false) =>
  str ? "3.1em" : CARD_WIDTH * 0.075;

export const imageWidth = CARD_WIDTH * 0.5;
export const imageHeight = CARD_HEIGHT * 0.2;
export const imageLeftValue = CARD_WIDTH * 0.35;
export const imageTopValue = CARD_WIDTH * 0.16;

export const headerCornersWidth = CARD_WIDTH * 0.2;

export const classIconSize = CARD_WIDTH * 0.18;
export const levelFontSize = "3em";
export const levelFontSizeNum = CARD_WIDTH * 0.06;
export const levelLeftValue = (str = false) =>
  str ? "2.3em" : CARD_WIDTH * 0.875;
export const levelTopValue = (str = false) =>
  str ? "2.5em" : CARD_HEIGHT * 0.16;

export const titleWidth = (str = false) =>
  str ? `${CARD_WIDTH * 0.55}px` : CARD_WIDTH * 0.55;
export const titleFontSize = `${CARD_WIDTH * 0.06}px`;

export const descWidth = `${CARD_WIDTH * 0.9}px`;
export const descIconSize = (str = false) =>
  str ? "1.8em" : CARD_WIDTH * 0.08;
export const descFontSize = "2.5em";

export const resourcesHeight = "6em";
export const resourcesSize = (str = false) =>
  str ? "2.5em" : CARD_WIDTH * 0.12;

export function processMarkdown({
  content,
  // regex,
  html = false,
}) {
  const regex = JSON.parse(localStorage.getItem("ifcRegex") ?? "");

  let ret = marked.parse(content);

  const getIconImage = (type = "") => {
    switch (type) {
      case "p":
        return regex["Poison"];
      case "d":
        return regex["Damage"];
      case "b":
        return regex["Block"];
      case "o":
        return regex["Pain"];
      case "h":
        return regex["Heal"];
      default:
        return "";
    }
  };

  ret = ret
    .replaceAll(/\^[dbpho][\+0-9xX\+\-\*]*\^/g, (value) => {
      const type = value.charAt(1);
      // const { iconSize, fontSize } = getIcon(value);
      return valueIconHtml
        .replaceAll("xxVALUExx", value.slice(2, -1))
        .replaceAll("xxIMAGExx", getIconImage(type))
        .replaceAll("xxSIZExx", descIconSize(true));
    })
    // REGEX WORDS to ICONS
    .replaceAll(/\^[a-zA-Z]+\^/g, (value) => {
      const src = regex[value.slice(1, -1)];
      return iconHtml
        .replaceAll("xxIMAGExx", src)
        .replaceAll("xxSIZExx", descIconSize(true));
    });

  const retValue = emboldenKeywords(ret);
  if (retValue.includes("imagedelivery") && retValue.includes("<img")) {
    // <img src=""https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/dreya_graviticbola/public"">
    const imgIndex =
      retValue.slice(retValue.indexOf("<img") + 4, -1).indexOf(">") +
      retValue.indexOf("<img") +
      3;
    return `${retValue.slice(
      0,
      imgIndex - 7 // we subtract seven here to include /public in what gets replaced
    )}/quality=100,width=400" width=400 height=400${retValue.slice(
      imgIndex,
      retValue.length
    )}`;
  } else {
    return retValue;
  }
}

const processBox = ({ content, label, color, bgColor, type = "slotUse" }) => {
  const html =
    type === "ifthen"
      ? ifThenBoxHtml
      : type === "special"
      ? reactionBoxHtml
      : boxHtml;
  return html
    .replaceAll("xxBGCOLORxx", bgColor)
    .replaceAll("xxCOLORxx", color)
    .replaceAll("xxLABELxx", label)
    .replaceAll("xxWIDTHxx", descWidth)
    .replaceAll("xxCONTENTxx", content);
};

export const createCardDescription = ({ card }) => {
  const onSlot = card.OnSlot
    ? processBox({
        content: card.OnSlot,
        label: "Slot",
        color: "blue",
        bgColor: "rgba(0,0,255,.2)",
      })
    : "";
  const onUse = card.OnUse
    ? processBox({
        content: card.OnUse,
        label: "Use",
        color: "black",
        bgColor: "rgba(0,0,0,.05)",
      })
    : "";
  const reaction = card.Reaction
    ? processBox({
        type: "ifthen",
        content: card.ReactionAction,
        label: card.Reaction,
        color: "red",
        bgColor: "rgba(255,0,0,.2)",
      })
    : "";
  const special = card.Special
    ? processBox({
        type: "special",
        content: card.SpecialAction,
        label: card.Special,
        color: "green",
        bgColor: "rgba(0,255,0,.2)",
      })
    : "";
  return `
  ${processMarkdown({ content: onSlot })}
  ${processMarkdown({ content: onUse })}
  ${processMarkdown({
    content: reaction,
  })}
  
  ${processMarkdown({
    content: special,
  })}`;
};

export const createCardTitle = ({ card }) => {
  return cardTitleHtml
    .replaceAll("xxFONTSIZExx", titleFontSize)
    .replaceAll("xxWIDTHxx", titleWidth(true))
    .replaceAll("xxTITLExx", card.Title);
};
