/* eslint-disable import/no-webpack-loader-syntax */
import React, { useContext } from "react";
import ClassIcon from "./ClassIcon";
import { classColors } from "../utility/colors";
import {
  CARD_HEIGHT,
  CARD_WIDTH,
  classIconSize,
  createCardDescription,
  descFontSize,
  headerCornersWidth,
  imageHeight,
  imageWidth,
  levelFontSize,
  levelLeftValue,
  levelTopValue,
  processMarkdown,
  resourcesHeight,
  resourcesSize,
  sparkBorderWidth,
  sparkFontSize,
  sparkLeftValue,
  sparkSize,
  sparkTopValue,
  titleFontSize,
  titleWidth,
} from "./cardUtil";
import { AppContext } from "../context/AppContext";

export default function Card({ card, regex: passedRegex = undefined }) {
  const { state: { regex: appRegex } } = useContext(AppContext);
  const regex = passedRegex ?? appRegex;
  return (
    <div
      style={{
        position: "relative",
        border: `1em solid ${classColors[card.Class]}`,
        margin: 8,
        borderRadius: "3em",
        width: CARD_WIDTH,
        height: CARD_HEIGHT,
        backgroundColor: "#fff",
        display: "flex",
        flexDirection: "column",
        overflow: "hidden",
        overflowX: "auto",
        overflowY: "auto",
      }}
    >
      {/* HEADERRRRR */}
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          marginTop: "1em",
          height: "7em",
        }}
      >
        <div style={{ width: headerCornersWidth }} />
        <div
          style={{
            textAlign: "center",
            color: "black",
            width: titleWidth(),
            fontSize: titleFontSize,
            fontWeight: "600",
          }}
          dangerouslySetInnerHTML={{
            __html: processMarkdown({
              content: card["Title"],
            }),
          }}
        />
        <div
          style={{
            width: headerCornersWidth,
            display: "flex",
            justifyContent: "flex-end",
            marginRight: 6,
            position: "relative",
          }}
        >
          <ClassIcon
            className={card["Class"]}
            color={classColors[card.Class]}
            size={classIconSize}
          />
          <div
            style={{
              color: "black",
              fontSize: levelFontSize,
              fontWeight: "bold",
              position: "absolute",
              // right: levelRightValue,
              zIndex: 1,
              top: levelTopValue(true),
              left: levelLeftValue(true),
            }}
          >
            {card["Level"]}
          </div>
        </div>
      </div>

      {/* IMAGEEEEEE */}
      <div style={{ display: "flex", justifyContent: "center" }}>
        {card["Image"]?.length > 0 ? (
          <img
            src={card["Image"]}
            alt="card art"
            width={imageWidth}
            height={imageHeight}
            style={{ objectFit: "contain" }}
          />
        ) : (
          <div
            style={{
              width: imageWidth,
              height: imageHeight,
              borderRadius: 12,
              backgroundColor: "#f9f9f9",
            }}
          />
        )}
      </div>

      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          alignItems: "center",
          marginTop: '1em'
        }}
      >
        <div
          style={{
            marginLeft: 12,
            marginRight: 12,
            textAlign: "center",
          }}
          dangerouslySetInnerHTML={{
            // __html: getCardDescriptionHtml({ card, regex, html: true }).desc,
            __html: createCardDescription({ card }),
          }}
        />
        {/* <div
          style={{
            marginTop: 6,
            marginLeft: 12,
            marginRight: 12,
            textAlign: "center",
          }}
          dangerouslySetInnerHTML={{
            __html: getCardDescriptionHtml({ card, regex, html: true }).trigger,
          }}
        /> */}
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          position: "absolute",
          top: sparkTopValue,
          left: sparkLeftValue,
        }}
      >
        <Spark card={card} regex={regex} />
      </div>

      <div style={{ display: "flex", width: "100%", height: resourcesHeight }}>
        <Stats card={card} regex={regex} />
      </div>
    </div>
  );
}


function Spark({ card, regex }) {
  const sp = parseInt(card.Spark);
  if (!Number.isNaN(sp) && Number.isInteger(sp)) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <div
          style={{
            padding: 2,
            border: `${sparkBorderWidth}px solid black`,
            display: "flex",
            width: sparkSize,
            height: sparkSize,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 80,
          }}
        >
          <span
            style={{
              fontWeight: "bold",
              fontSize: sparkFontSize,
              color: "black",
              textAlign: "center",
            }}
          >
            {sp}
          </span>
        </div>
      </div>
    );
  }
  return null;
}

// function Stats({ card, regex }) {
//   console.log('zzzz', )
//     let result = ''
//   icon = `${new Array(cost.length).fill(type).map(i => getImage(regex['Power']))}`
//     return (
//     <div dangerouslySetInnerHTML={{ __html: result }} />
//   )
// }

// function Cost({ card, regex }) {
//   // const sp = parseInt(card.Spark);
//   const costs = card?.Cost?.split(',')
//   let result = '';
//   if (!costs || costs?.length === 0) return null;

//   const clearOtherValues = (str, labels) => {
//     let res = str
//     labels.forEach(l => res = res.replaceAll(l, ''))
//     return res;
//   }

//   const allTypes = ['xxANYxx', 'xxPOWERxx', 'xxSPEEDxx', 'xxTACTICSxx']

//   costs.forEach(cost => {
//     const type = cost.charAt(0)
//     const ret = costHtml;
//     let icon = ''
//     let label = ''
//     let getImage = src => `<img src="${src}" style="width:${costIconSize(true)};height:${costIconSize(true)};" />`
//     switch (type) {
//       case 'a':
//       case 'p':
//         icon = `${new Array(cost.length).fill(type).map(i => getImage(regex['Power']))}`
//         label = 'xxPOWERxx'
//         break;
//       case 's':
//         icon = `${new Array(cost.length).fill(type).map(i => getImage(regex['Speed']))}`
//         label = 'xxSPEEDxx'
//         break;
//       case 't':
//         icon = `${new Array(cost.length).fill(type).map(i => getImage(regex['Tactics']))}`
//         label = 'xxTACTICSxx'
//         break;
//     }
//     result = `${result}${ret.replaceAll(label, icon)}`
//     const toRemove = allTypes.filter(t => t !== label);
//     result = clearOtherValues(result, toRemove)
//   })
//   return (
//     <div dangerouslySetInnerHTML={{ __html: result }} />
//   )
// }

function Stats({ card, regex }) {
  // if (!card[type]) return null;
  const p = parseInt(card['Power']);
  const s = parseInt(card['Speed']);
  const t = parseInt(card['Tactics']);
  const num = p + s + t;
  const type = 'Spark';
  if (num === 0) {
    return null;
  }
  return (
    <div
      style={{
        display: "flex",
        flex: 1,
        justifyContent: "center",
        border: "2px solid gray",
        padding: "4px 0",
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {num > 3 && (
          <div>
            <img
              src={regex[type]}
              alt="card art"
              width={resourcesSize()}
              height={resourcesSize()}
              style={{ objectFit: "contain", verticalAlign: "text-bottom" }}
            />
            <span
              style={{
                fontWeight: "bold",
                marginLeft: 6,
                fontSize: descFontSize,
                color: "black",
              }}
            >
              {num}
            </span>
          </div>
        )}
        {num <= 3 &&
          new Array(num)
            .fill(0)
            .map((item, i) => (
              <img
                key={i}
                src={regex[type]}
                alt="card art"
                width={resourcesSize()}
                height={resourcesSize()}
                style={{ objectFit: "contain" }}
              />
            ))}
      </div>
    </div>
  );
}