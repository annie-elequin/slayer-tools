import { Rect, Text, Image, Group, Line } from "react-konva";
import useImage from "use-image";
import { classColors } from "../../utility/colors";
import { getClassIconUrl } from "../ClassIcon";
import {
  CARD_HEIGHT,
  CARD_WIDTH,
  classIconSize,
  imageHeight,
  imageLeftValue,
  imageTopValue,
  levelFontSizeNum,
  levelLeftValue,
  levelTopValue,
  resourcesSize,
  titleWidth,
} from "../cardUtil";
import React from "react";

export default function CanvasCard({
  card,
  regex,
  descriptionUrl,
  titleUrl,
  xOffset = 0,
  yOffset = 0,
}) {
  const props = { card, regex, xOrigin: xOffset, yOrigin: yOffset };

  return (
    <Group>
      <Border {...props} />
      <CardName {...props} url={titleUrl} />
      <ClassImage {...props} />
      <Level {...props} />
      {card.Image?.trim().length > 0 && (
        <Img
          url={card.Image}
          x={xOffset + imageLeftValue}
          y={yOffset + imageTopValue}
          width={imageHeight}
          height={imageHeight}
        />
      )}
      <Img
        url={descriptionUrl}
        x={xOffset + CARD_WIDTH * 0.05}
        y={yOffset + CARD_HEIGHT * 0.35}
      />
      <Stats {...props} />
      <TodayDate {...props} />
      <Spark {...props} />
    </Group>
  );
}

const Img = ({ url, ...props }) => {
  const [image] = useImage(url, "Anonymous" as any);
  return <Image image={image} draggable {...props} />;
};

function Border({ card, xOrigin, yOrigin }) {
  return (
    <Rect
      x={xOrigin + CARD_WIDTH * 0.025}
      y={yOrigin + CARD_HEIGHT * 0.028}
      width={CARD_WIDTH - CARD_WIDTH * 0.05}
      height={CARD_HEIGHT - CARD_HEIGHT * 0.07}
      cornerRadius={10}
      strokeEnabled={true}
      strokeWidth={CARD_WIDTH * 0.02}
      stroke={classColors[card.Class] || "#777"}
    />
  );
}

function CardName({ card, xOrigin, yOrigin, url }) {
  return (
    <Img
      x={xOrigin + CARD_WIDTH * 0.21}
      y={yOrigin + CARD_HEIGHT * 0.04}
      width={titleWidth()}
      draggable
      url={url}
    />
  );
}

function ClassImage({ card, xOrigin, yOrigin }) {
  return (
    <Img
      url={getClassIconUrl(card.Class)}
      width={classIconSize}
      height={classIconSize}
      x={xOrigin + CARD_WIDTH * 0.76}
      y={yOrigin + CARD_HEIGHT * 0.04}
      draggable
    />
  );
}

function Level({ card, xOrigin, yOrigin }) {
  return (
    <Text
      text={card.Level}
      x={xOrigin + levelLeftValue()}
      y={yOrigin + levelTopValue()}
      fontSize={levelFontSizeNum}
      fontVariant="bold"
      draggable
    />
  );
}

function TodayDate({ xOrigin, yOrigin }) {
  const d = new Date().toLocaleDateString("en-us", {
    year: "numeric",
    month: "short",
    day: "numeric",
  });
  return (
    <Text
      text={d}
      x={xOrigin + CARD_WIDTH * 0.07}
      y={yOrigin + CARD_HEIGHT * 0.93}
      fontSize={CARD_WIDTH * 0.02}
      draggable
    />
  );
}

function Spark({ card, xOrigin, yOrigin }) {
  return (
    <>
      <Rect
        x={xOrigin + CARD_WIDTH * 0.065}
        y={yOrigin + CARD_HEIGHT * 0.045}
        width={CARD_WIDTH * 0.15}
        height={CARD_WIDTH * 0.15}
        strokeEnabled
        strokeWidth={10}
        stroke={classColors[card.Class] || "#777"}
        cornerRadius={100}
      />
      <Text
        text={card.Spark}
        x={xOrigin + CARD_WIDTH * 0.111}
        y={yOrigin + CARD_HEIGHT * 0.067}
        fontSize={CARD_WIDTH * 0.1}
        fontVariant="bold"
        draggable
      />
    </>
  );
}

function Stats({ card, regex, xOrigin, yOrigin }) {
  const p = parseInt(card["Power"]);
  const s = parseInt(card["Speed"]);
  const t = parseInt(card["Tactics"]);
  const num = p + s + t;

  if (num === 0) {
    return null;
  }

  return (
    <>
      <Line
        x={xOrigin + CARD_WIDTH * 0.04}
        y={yOrigin + CARD_HEIGHT * 0.82}
        points={[0, 0, CARD_WIDTH * 0.92, 0]}
        stroke="#ccc"
        strokeWidth={4}
      />
      <Rect
        x={xOrigin + CARD_WIDTH * 0.465}
        y={yOrigin + CARD_HEIGHT * 0.8}
        width={CARD_WIDTH * 0.095}
        height={CARD_WIDTH * 0.06}
        cornerRadius={10}
        fill="white"
      />
      <Text
        text={"OR"}
        x={xOrigin + CARD_WIDTH * 0.48}
        y={yOrigin + CARD_HEIGHT * 0.81}
        fontSize={CARD_WIDTH * 0.04}
        draggable
      />
      <Img
        x={xOrigin + CARD_WIDTH * 0.42}
        y={yOrigin + CARD_HEIGHT * 0.85}
        width={resourcesSize(false)}
        height={resourcesSize(false)}
        draggable
        url={regex["Spark"]}
      />
      <Text
        text={`${num}`}
        x={xOrigin + CARD_WIDTH * 0.55}
        y={yOrigin + CARD_HEIGHT * 0.865}
        fontSize={CARD_WIDTH * 0.07}
        draggable
      />
    </>
  );
}
