import React, { useContext } from 'react'
import { useRoute } from 'wouter';
import { AppContext } from '../../context/AppContext';
import Card from '../../components/HtmlCard';

export default function Embed() {
  const { state: { publicAbilities: abilities, regex }} = useContext(AppContext);
  const [match, params] = useRoute("/embed/:className");

  let shownAbilities = abilities;
  if (match) {
      shownAbilities = abilities.filter(a => a["Class"].toLowerCase() === params.className)
  }

  return (
    <div>
      <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', backgroundColor: '#031728' }}>
        {
        shownAbilities
        .filter(c => c['Public'] === 'YES')
        .map(a => <Card card={a} regex={regex} />)}
      </div>
    </div>
  )
}