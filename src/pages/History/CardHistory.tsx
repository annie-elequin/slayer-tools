// https://gitlab.com/annie-elequin/ifctools/-/blob/main/nathan/Cards/OverheadSlam.txt
// /projects/:id/repository/commits?path=:file_path
// curl --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/ifctools/repository/commits"
//
import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import { useRoute } from "wouter";
import { getCardDataAtCommit, getCommitsForCard } from "../../api";
import Card from "../../components/HtmlCard";
import { AppContext } from "../../context/AppContext";

export default function CardHistory() {
  const {
    state: { regex },
  } = useContext(AppContext);
  const [, params] = useRoute("/history/:cardId");
  const [commitResults, setCommitResults] = useState<any>();
  const prevCardId = useRef<any>();

  const getData = useCallback(async () => {
    setCommitResults([]);
    const commits = await getCommitsForCard(params?.cardId);
    const promises: any[] = [];
    commits
    .filter(c => c.committer_name?.includes('Nathan'))
    .filter(c => c.title !== 'na')
    .forEach((commit) => {
      console.log('Commit for card: ', {commit})
      const committedDate = new Date(commit.committed_date);
      const newerThan = new Date("2023-01-31T23:41:42.000-06:00");
      if (committedDate.getTime() > newerThan.getTime()) {
        promises.push(getCardDataAtCommit(commit.id, params?.cardId));
      }
    });
    const results = await Promise.allSettled(promises);
    const cards: any[] = [];
    results
      .filter((r) => r.status === "fulfilled")
      .forEach((res, i) => {
        // @ts-ignore
        const result = res.value;
        const card = buildCard(result.data);
        // @ts-ignore
        card.commitMessage = commits[i]?.message;
        // @ts-ignore
        card.commitDate = commits[i]?.committed_date;
        // @ts-ignore
        card.commitLink = commits[i]?.web_url;
        cards.push(card);
      });
    console.log("Resulting cards: ", cards);
    setCommitResults(cards);
  }, [params]);

  useEffect(() => {
    if (params?.cardId && params.cardId !== prevCardId.current) {
      prevCardId.current = params.cardId;
      getData();
    }
  }, [params, getData]);

  return (
    <div style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}>
      {commitResults?.map((card) => (
        <div
          style={{
            width: 350,
            height: 550,
            color: "white",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            textAlign: "center",
            backgroundColor: "#ffffff10",
            borderRadius: 10,
            padding: 12,
            margin: 12,
          }}
        >
          <div
            style={{
              transform: "scale(.35)",
              transformOrigin: "50% 0% 0px",
            }}
          >
            <Card card={card} regex={regex} />
          </div>
          <div>
            {card.commitMessage}
            <br />
            <br />
            {new Date(card.commitDate).toLocaleDateString("en-us", {
              weekday: "long",
              year: "numeric",
              month: "short",
              day: "numeric",
            })}
            <br />
            {new Date(card.commitDate).getHours()}:
            {new Date(card.commitDate).getMinutes()}
            <br />
            <a
              href={card.commitLink}
              style={{
                color: "teal",
                textDecoration: "underline",
                cursor: "pointer",
              }}
            >
              View in web
            </a>
          </div>
        </div>
      ))}
      {commitResults?.length === 0 && <div>No results</div>}
    </div>
  );
}

function buildCard(rawData) {
  return {
    Title: rawData.match(/Title\n(.*)\n\n/g)?.[0]?.slice(6, -2) || "",
    Class: rawData.match(/Class\n(.*)\n\n/g)?.[0]?.slice(6, -2) || "",
    Level: rawData.match(/Level\n(.*)\n\n/g)?.[0]?.slice(6, -2) || "",
    Type: rawData.match(/Type\n(.*)\n\n/g)?.[0]?.slice(5, -2) || "",
    Scale: rawData.match(/Scale\n(.*)\n\n/g)?.[0]?.slice(7, -2) || "",
    Image: rawData.match(/Image\n(.*)\n\n/g)?.[0]?.slice(6, -2) || "",
    Quantity: rawData.match(/Quantity\n(.*)\n\n/g)?.[0]?.slice(9, -2) || "",
    Spark: rawData.match(/Spark\n(.*)\n\n/g)?.[0]?.slice(6, -2) || "",
    Description:
      rawData.match(/Description\n(.*)Keep/gms)?.[0]?.slice(12, -6) || "",
    Keep: rawData.match(/Keep\n(.*)\n\n/g)?.[0]?.slice(5, -2) || "",
    Trigger: rawData.match(/Trigger\n(.*)\n\n/g)?.[0]?.slice(8, -2) || "",
    TriggerAction:
      rawData.match(/TriggerAction\n(.*)\n\n/g)?.[0]?.slice(14, -2) || "",
    Print: rawData.match(/Print\n(.*)\n\n/g)?.[0]?.slice(6, -2) || "",
    Public: rawData.match(/Public\n(.*)\n\n/g)?.[0]?.slice(7, -2) || "",
    id: rawData.match(/id\n(.*)\n\n/g)?.[0]?.slice(3, -2) || "",
  };
}
