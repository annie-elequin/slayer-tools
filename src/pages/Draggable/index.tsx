import React, { useCallback, useContext, useEffect, useState } from "react";
import styles from './styles.module.css';
import { useDrop } from 'react-dnd'
import update from 'immutability-helper'
import { Box } from "./Box";
import classNames from "classnames";
import { AppContext } from "../../context/AppContext";
import Header from "../../Header";
import { transformCardStyle } from "../../utility/styles";

export default function DraggablePage() {
  const { state: { publicAbilities: abilities, regex }} = useContext(AppContext);

  const [boxes, setBoxes] = useState({})
  const moveBox = useCallback(
    (id, left, top) => {
      const b = update(boxes, {
        [id]: {
          $merge: { left, top },
        },
      })
      localStorage.setItem('draggable-items', JSON.stringify(b))
      setBoxes(b)
    },
    [boxes],
  )
  const [, drop] = useDrop(
    () => ({
      accept: 'card',
      drop(item: any, monitor) {
        const delta: any = monitor.getDifferenceFromInitialOffset()
        let left = Math.round(item.left + delta.x)
        let top = Math.round(item.top + delta.y)
        moveBox(item.id, left, top)
        return undefined
      },
    }),
    [moveBox],
  )

  useEffect(() => {
      if (!!localStorage.getItem('draggable-items')) return;
      if (abilities.length === 0) return;
      const defaultBoxes = boxes;
      abilities.forEach(a => {
        if (Object.keys(defaultBoxes).length > 0) {
            const c = defaultBoxes[a['Title']]
            if (c) {
                defaultBoxes[a['Title']].card = a;
                defaultBoxes[a['Title']].title = a['Title'];
            } else {
                defaultBoxes[a['Title']] = { top: 50 + (parseInt(a['Level']) * 450), left: 0, title: a['Title'], card: a}
            }
        } else {
            defaultBoxes[a['Title']] = { top: 50 + (parseInt(a['Level']) * 450), left: 0, title: a['Title'], card: a}
        }
      })
      setBoxes(defaultBoxes);
      localStorage.setItem('draggable-items', JSON.stringify(defaultBoxes));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [abilities])

  useEffect(() => {
      const storedBoxesjson = localStorage.getItem('draggable-items')
      if (!storedBoxesjson) return;
      console.log('About to parse json:')
      console.log(storedBoxesjson)
      const storedBoxes = JSON.parse(storedBoxesjson || '')
      setBoxes(storedBoxes)
  }, [])
  
  return (
    <div>
      <Header />
 <div ref={drop} className={classNames(styles.wrapper)} style={{ ...transformCardStyle }}>
      {Object.keys(boxes).map((key, i) => {
        const { left, top, title, card } = boxes[key]
        return (
          <Box
            key={i}
            id={key}
            left={left}
            top={top}
            hideSourceOnDrag={true}
            card={card}
            regex={regex}
          >
            {title}
          </Box>
        )
      })}
    </div>
    </div>
   
  )
}