import { useState } from "react";
import Header from "../../Header";
import { Button, FormInput } from "shards-react";
import { uploadImageToCloudflare } from "../../api";

export default function Upload() {
  const [id, setId] = useState('')
  const [file, setFile] = useState()
  const [image, setImage] = useState<any>();
  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      setFile(event.target.files[0])
      setImage(URL.createObjectURL(event.target.files[0]));
    }
  };
  const upload = () => {
    uploadImageToCloudflare({ file, id })
  };
  return (
    <div>
      <Header />
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          paddingTop: 32,
        }}
      >
        <input
          type="file"
          accept="image/*"
          onChange={onImageChange}
          style={{ marginBottom: 32 }}
        />
        {!!image && (
          <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <FormInput placeholder='Image ID' value={id} onChange={e => setId(e.target.value)} style={{ marginRight: 12 }} />
            <Button onClick={upload}>Upload</Button>
          </div>
        )}
        {!!image && <img alt='preview' src={image} style={{ marginTop: 32, maxWidth: 500 }} />}
      </div>
    </div>
  );
}
