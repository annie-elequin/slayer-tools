import React, { useContext } from "react";
import { useRoute } from "wouter";
import Card from "../../components/HtmlCard";
import { AppContext } from "../../context/AppContext";
import Header from "../../Header";
import { transformCardStyle } from "../../utility/styles";

export default function Viewer() {
  const {
    state: { publicAbilities: abilities, regex },
  } = useContext(AppContext);
  const [classMatch, classParam] = useRoute("/cards/:className");
  const [classIdMatch, classIdParam] = useRoute("/cards/:className/:cardId");

  let shownAbilities = abilities;
  if (classMatch) {
    shownAbilities = abilities.filter(
      (a) => a["Class"].toLowerCase() === classParam.className
    );
  }

  if (classIdMatch) {
    shownAbilities = abilities.filter(
      (a) => a["id"].toLowerCase() === classIdParam.cardId
    );
  }

  return (
    <div>
      <Header />
      <div
        style={{
          display: "grid",
          justifyContent: "center",
          gridTemplateColumns: "repeat(auto-fit, 850px",
          gap: "3em",
          columnGap: "1em",
          width: "275vw",
          ...transformCardStyle,
          marginTop: "2em",
          marginLeft: "2em",
        }}
      >
        {shownAbilities
          .map((a) => (
            <Card key={a.id} card={a} regex={regex} />
          ))}
      </div>
    </div>
  );
}
