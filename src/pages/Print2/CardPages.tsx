/* eslint-disable */

import React, { useEffect, useRef, useState } from "react";
import { Stage, Layer, Rect, Text, Image, Line } from "react-konva";
import { Button, FormInput } from "shards-react";
import useImage from "use-image";
import domtoimage from 'dom-to-image-more';
import { classColors } from "../../utility/colors";
import { processCardDescriptionMarkdown } from "../../utility";
import { getClassIconUrl } from "../../components/ClassIcon";
import CanvasCard from "../../components/CanvasCard/CanvasCard";
import { transformCardStyle } from "../../utility/styles";
import { CARD_HEIGHT, CARD_WIDTH, DPI, createCardDescription, createCardTitle, descWidth, titleWidth } from "../../components/cardUtil";
const { default: descriptionHtml } = require('raw-loader!../../components/CanvasCard/BoxHtml.html');

export default function CardImage({ data, regex, curPage, clearElements, column = "4", row = "2" }) {
  const [rows] = useState(row);
  const [col] = useState(column);
  const [desc, setDesc] = useState({});
  const [titles, setTitles] = useState({});
  const [renderReady, setRenderReady] = useState(false);

  const stage = useRef(null);

  useEffect(() => {
    if (data && renderReady) {
      getDescriptions();
    }
  }, [renderReady]);

  useEffect(() => {
    setRenderReady(false);
    clearElements();
  }, [curPage]);

  const download = async () => {
    if (stage.current) {
      // @ts-ignore
      const url = stage.current.toDataURL();
      console.log({ url });
      var link = document.createElement("a");
      link.download = "cards.png";
      link.href = url;
      link.click();
    }
  };

  const getDescriptions = async () => {
    clearElements();

    const idMap = {};
    const promises: any[] = [];
    data.forEach(async (card, i) => {
      const div = document.createElement("div");
      div.id = card.id;
      div.style.width = descWidth;

      const desc = createCardDescription({ card })

      div.innerHTML = desc;
      document.getElementById("root")?.appendChild(div);
      promises.push(
        domtoimage.toPng(div, {
          cacheBust: true
        })
      );
      idMap[i] = card.id;
    });
    const descriptions = {};
    let result: any = undefined
    try {
      result = await Promise.all(promises);
    } catch (e: any) {
      alert('Something went wrong')
      alert(JSON.stringify(e))
    }
    console.log('Desc Result: ', {result})
    if (!result) return;
    result.forEach((r, i) => {
      descriptions[idMap[i]] = r;
    });
    setDesc(descriptions);
    await getTitles();
  };

  const getTitles = async () => {
    const idMap = {};
    const promises: any[] = [];
    data.forEach(async (card, i) => {
      const div = document.createElement("div");
      div.id = `title-${card.id}`;
      div.style.width = titleWidth(true) as string;

      const title = createCardTitle({ card })


      div.innerHTML = `${title}`;
      document.getElementById("root")?.appendChild(div);
      promises.push(
        domtoimage.toPng(div, {
          cacheBust: true,
        })
      );
      idMap[i] = `title-${card.id}`;
    });
    const newTitles = {};
    const result = await Promise.all(promises);
    result.forEach((r, i) => {
      newTitles[idMap[i]] = r;
    });
    setTitles(newTitles);
  };

  return !data ? null : (
    <>
      <h4 style={{ color: "white" }}>Canvas</h4>

      <div style={{ paddingBottom: 48 }}>
        <Button style={{ marginRight: 8 }} onClick={download}>
          Download
        </Button>
        {/* <Button onClick={clearElements} style={{ marginRight: 8 }}>Clear</Button> */}
        <Button onClick={() => setRenderReady(true)}>Render Cards</Button>
      </div>

      {renderReady && (
        <div
          style={{
            width: parseInt(col) * CARD_WIDTH,
            height: parseInt(rows) * CARD_HEIGHT,
            marginBottom: 100,
            border: "1px solid black",
            backgroundColor: "white",
            ...transformCardStyle,
            transform: "scale(.20)",
          }}
        >
          <Stage ref={stage} width={parseInt(col) * 3 * DPI} height={parseInt(row) * 4 * DPI}>
            <Layer>
              {data.map((card, index) => {
                const xOffset = ((index % parseInt(col))) * CARD_WIDTH;
                const yOffset = Math.floor(index / parseInt(col)) * CARD_HEIGHT;

                return (
                  <CanvasCard
                    card={card}
                    regex={regex}
                    descriptionUrl={desc[card.id]}
                    titleUrl={titles[`title-${card.id}`]}
                    xOffset={xOffset}
                    yOffset={yOffset}
                  />
                );
              })}
            </Layer>
          </Stage>
        </div>
      )}
    </>
  );
}

const Img = ({ url, ...props }) => {
  const [image] = useImage(url, "Anonymous" as any);
  return <Image image={image} draggable {...props} />;
};

function Border({ card, xOrigin, yOrigin }) {
  return (
    <Rect
      x={xOrigin + CARD_WIDTH * 0.038}
      y={yOrigin + CARD_HEIGHT * 0.028}
      width={CARD_WIDTH - CARD_WIDTH * 0.09}
      height={CARD_HEIGHT - CARD_HEIGHT * 0.07}
      cornerRadius={10}
      strokeEnabled={true}
      strokeWidth={CARD_WIDTH * 0.02}
      stroke={classColors[card.Class] || "#777"}
    />
  );
}

function CardName({ card, xOrigin, yOrigin, url }) {
  return (
    <Img
      x={xOrigin + CARD_WIDTH * 0.15}
      y={yOrigin + CARD_HEIGHT * 0.04}
      fontSize={CARD_WIDTH * 0.06}
      width={CARD_WIDTH * 0.65}
      draggable
      url={url}
    />
  );
}

function ClassImage({ card, xOrigin, yOrigin }) {
  return (
    <Img
      url={getClassIconUrl(card.Class)}
      width={CARD_WIDTH * 0.1}
      height={CARD_WIDTH * 0.1}
      x={xOrigin + CARD_WIDTH * 0.82}
      y={yOrigin + CARD_HEIGHT * 0.06}
      draggable
    />
  );
}

function Level({ card, xOrigin, yOrigin }) {
  return (
    <Text
      text={card.Level}
      x={xOrigin + CARD_WIDTH * 0.85}
      y={yOrigin + CARD_HEIGHT * 0.14}
      fontSize={CARD_WIDTH * 0.06}
      fontVariant="bold"
      draggable
    />
  );
}

function Stats({ card, regex, xOrigin, yOrigin }) {
  return (
    <>
      <Line
        x={xOrigin + CARD_WIDTH * 0.04}
        y={yOrigin + CARD_HEIGHT * 0.83}
        points={[0, 0, CARD_WIDTH * 0.9, 0]}
        stroke="#888"
        strokeWidth={8}
      />
      <Img
        x={xOrigin + CARD_WIDTH * 0.13}
        y={yOrigin + CARD_HEIGHT * 0.85}
        width={CARD_WIDTH * 0.08}
        height={CARD_WIDTH * 0.08}
        draggable
        url={regex["Power"]}
      />
      <Text
        text={card["Power"]}
        x={xOrigin + CARD_WIDTH * 0.23}
        y={yOrigin + CARD_HEIGHT * 0.85}
        fontSize={CARD_WIDTH * 0.07}
        draggable
      />
      {/* ******************* */}
      <Img
        x={xOrigin + CARD_WIDTH * 0.42}
        y={yOrigin + CARD_HEIGHT * 0.85}
        width={CARD_WIDTH * 0.08}
        height={CARD_WIDTH * 0.08}
        draggable
        url={regex["Speed"]}
      />
      <Text
        text={card["Speed"]}
        x={xOrigin + CARD_WIDTH * 0.52}
        y={yOrigin + CARD_HEIGHT * 0.85}
        fontSize={CARD_WIDTH * 0.07}
        draggable
      />
      {/* ******************* */}
      <Img
        x={xOrigin + CARD_WIDTH * 0.72}
        y={yOrigin + CARD_HEIGHT * 0.85}
        width={CARD_WIDTH * 0.08}
        height={CARD_WIDTH * 0.08}
        draggable
        url={regex["Tactics"]}
      />
      <Text
        text={card["Tactics"]}
        x={xOrigin + CARD_WIDTH * 0.82}
        y={yOrigin + CARD_HEIGHT * 0.85}
        fontSize={CARD_WIDTH * 0.07}
        draggable
      />
    </>
  );
}

function TodayDate({ xOrigin, yOrigin }) {
  const d = new Date().toLocaleDateString("en-us", {
    year: "numeric",
    month: "short",
    day: "numeric",
  });
  return (
    <Text
      text={d}
      x={xOrigin + CARD_WIDTH * 0.07}
      y={yOrigin + CARD_HEIGHT * 0.93}
      fontSize={CARD_WIDTH * 0.02}
      draggable
    />
  );
}

function Spark({ card, xOrigin, yOrigin }) {
  return (
    <>
      <Rect
        x={xOrigin + CARD_WIDTH * 0.065}
        y={yOrigin + CARD_HEIGHT * 0.045}
        width={CARD_WIDTH * 0.15}
        height={CARD_WIDTH * 0.15}
        strokeEnabled
        strokeWidth={10}
        stroke={classColors[card.Class] || "#777"}
        cornerRadius={100}
      />
      <Text
        text={card.Spark}
        x={xOrigin + CARD_WIDTH * 0.111}
        y={yOrigin + CARD_HEIGHT * 0.067}
        fontSize={CARD_WIDTH * 0.1}
        fontVariant="bold"
        draggable
      />
    </>
  );
}
