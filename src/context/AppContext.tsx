import React, { useReducer } from "react";

type AppState = {
  abilities: any[];
  publicAbilities: any[];
  nonDuplicatedAbilities: any[];
  regex: any;
  keywords: string[];
};

const initialState: AppState = {
  abilities: [],
  publicAbilities: [],
  nonDuplicatedAbilities: [],
  regex: {},
  keywords: [],
};

export enum actions {
  setAbilities,
  setPublicAbilities,
  setNonDuplicatedAbilities,
  setRegex,
  setKeywords
}

export const AppContext = React.createContext({
  state: initialState,
  dispatch: (action) => {},
});

export default function AppContextProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
}

function reducer(state: AppState, action) {
  switch (action.action) {
    case actions.setAbilities:
      return { ...state, abilities: action.payload };
    case actions.setPublicAbilities:
      return { ...state, publicAbilities: action.payload };
    case actions.setNonDuplicatedAbilities:
      return { ...state, nonDuplicatedAbilities: action.payload };
    case actions.setRegex:
      return { ...state, regex: action.payload };
    case actions.setKeywords:
      return { ...state, keywords: action.payload };
    default:
      return state;
  }
}
