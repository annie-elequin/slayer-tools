import Canvas2 from "./pages/Print2";
import Viewer from "./pages/Viewer";
import { Route, Switch } from "wouter";
import { useContext, useEffect } from "react";
import { actions, AppContext } from "./context/AppContext";
import { fetchIfcAbilities, fetchIfcAbilityRegex, fetchIfcKeywords } from "./api";
import DraggablePage from "./pages/Draggable";
import Embed from "./pages/Embed";
import History from "./pages/History";
import Test from "./pages/Test";
import HandSimulator from "./pages/HandSimulator";
import Tts from "./pages/TTS";
import Upload from "./pages/Upload";
import Compare from "./pages/Compare";
import Home from "./pages/Home";

export default function Router() {
  const { dispatch } = useContext(AppContext);

  const fetchStuff = async () => {
    const [abilities, regex, keywords] = await Promise.all(
      [fetchIfcAbilities(), fetchIfcAbilityRegex(), fetchIfcKeywords()]
    );
    const { allAbilities, nonDuplicatedAbilities } = abilities as any;
    dispatch({ action: actions.setAbilities, payload: allAbilities });
    dispatch({
      action: actions.setPublicAbilities,
      payload: allAbilities.filter((c) => c.Public === "YES"),
    });
    dispatch({
      action: actions.setNonDuplicatedAbilities,
      payload: nonDuplicatedAbilities.filter((c) => c.Public === "YES"),
    });
    dispatch({ action: actions.setRegex, payload: regex });
    dispatch({ action: actions.setKeywords, payload: keywords });
    localStorage.setItem('ifcRegex', JSON.stringify(regex));
    localStorage.setItem('ifcKeywords', JSON.stringify(keywords));
  };

  useEffect(() => {
    fetchStuff();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Switch>
      <Route path="/" component={Home} />
      <Route path="/print" component={Canvas2} />

      <Route path="/cards" component={Viewer} />
      <Route path="/cards/:className" component={Viewer} />
      <Route path="/cards/:className/:cardId" component={Viewer} />

      <Route path="/embed" component={Embed} />
      <Route path="/embed/:className" component={Embed} />

      {/* <Route path="/print" component={Canvas} /> */}
      <Route path="/history" component={History} />
      <Route path="/history/:cardId" component={History} />

      <Route path="/draggable" component={DraggablePage} />

      <Route path="/test" component={Test} />

      <Route path="/handsimulator" component={HandSimulator} />

      <Route path="/tts" component={Tts} />

      <Route path="/upload" component={Upload} />

      <Route path="/compare" component={Compare} />
      <Route path="/compare/:cardId" component={Compare} />
    </Switch>
  );
}
