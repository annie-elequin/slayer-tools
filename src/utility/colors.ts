export const classColors = {
  Azura: '#83fff6',
  Cassius: '#cd5bcd',
  Dreya: '#3068c9',
  Foulborn: '#569a39',
  Scourge: '#e53630',
  Tad: '#d9e8b2', 
}