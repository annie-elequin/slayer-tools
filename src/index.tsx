import React from "react";
import './index.css'
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css";
import AppContextProvider from "./context/AppContext";
import Router from "./Router";
import { createRoot } from 'react-dom/client';
import { HTML5Backend } from "react-dnd-html5-backend";
import { DndProvider } from "react-dnd";

const container = document.getElementById('root');
const root = createRoot(container); // createRoot(container!) if you use TypeScript
root.render(
  <React.StrictMode>
    <AppContextProvider>
      <DndProvider backend={HTML5Backend}>
        <Router />
      </DndProvider>
    </AppContextProvider>
  </React.StrictMode>
);