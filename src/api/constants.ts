// const { currentVersion } = require('../../gameConfig.js');
import gameConfig from "../gameConfig"
const { currentVersion, mercenaries, compare } = gameConfig;

export const fetchRawFileUrl = `https://gitlab.com/api/v4/projects/24709529/repository/files/`
export const mercenaryPath = (m) => `${encodeURIComponent(`${currentVersion}/Mercenaries/${m}.csv`)}?ref=main`

export const mercs = mercenaries.map(m => `${fetchRawFileUrl}${mercenaryPath(m)}`)

export const backups = compare.map(m => `${fetchRawFileUrl}${mercenaryPath(m)}`)

export const ifcAbilityRegex = `https://docs.google.com/spreadsheets/d/e/2PACX-1vTwj8BT_xBiVUi5gYTsjlhxV6FVvgYtzkUTrCxVQCMKso26zhXKF9h8C-TrMEH8_V6DMco3rOFrNJ6Q/pub?gid=1365213155&single=true&output=csv&timestamp=${new Date().getTime()}`

export const ifcKeywords = `https://docs.google.com/spreadsheets/d/e/2PACX-1vRuMqDxvUN-h8MJHOVJaI5TUWiiR62y_kBcVdVYyKyiltQ4iLCnI5tgDGfcyHHB8bDThMrkuBkCIVEf/pub?gid=1630975163&single=true&output=csv&timestamp=${new Date().getTime()}`