Title
Counterfeit


Class
Cassius


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-4FTsMcc/0/d5843b04/O/download%20%2812%29.png


Quantity
1


Spark
3


Description
^Draw^Draw 2 Cards


Trigger
Always


TriggerAction
If you ^Discard^Discard this from your hand, swap it for the top card of your Discard pile


Print
NO


Public
YES


id
cas21


Power
0


Speed
1


Tactics
2






