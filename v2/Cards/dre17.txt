Title
Grappling Arrow


Class
Dreya


Level
S


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-nffwcLz/0/d27cd0e4/O/IFC%20%28Block%29%20%282%29.png


Quantity
1


Spark
1


Description
(^Line^Line 5): Target any Unit or Obstacle. Choose between <br/> <br/>^Pull^**Pull** the **Unit** to the closest unoccupied space or <br/><br/>^Push^**Push** yourself to the Target


Trigger



TriggerAction



Print
YES


Public
YES


id
dre17


Power
0


Speed
2


Tactics
0






