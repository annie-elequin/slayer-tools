Title
Knife Toss


Class
Cassius


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-mr2TsMD/0/e3b0167f/O/Knife%20Toss.png


Quantity
1


Spark
2


Description
(^Range^3): ^d4^<br/><br/>^Draw^Draw 1<br/>^Discard^Discard 1


Trigger



TriggerAction



Print
NO


Public
YES


id
cas29


Power
1


Speed
0


Tactics
1






