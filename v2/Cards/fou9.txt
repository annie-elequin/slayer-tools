Title
Shield Bash


Class
Foulborn


Level
S


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-JXFN4NZ/0/3d640a9c/O/shield-bash.png


Quantity
1


Spark
2


Description
Gain ^b5^ Block<br/>^Move^Move 0-2<br/><img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-6GJZNhJ/0/e3f8ca46/O/Foulborn_ShieldBash%20%286%29.png"> <br/>Lose 1-4 ^b^Block and deal equal ^dX^ Damage


Trigger



TriggerAction



Print
YES


Public
YES


id
fou9


Power
1


Speed
1


Tactics
1






