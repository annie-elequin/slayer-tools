Title
Drop Kick


Class
Foulborn


Level
1


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Foulborn/i-gvkbvMq/0/a518c80f/O/dropkick.png


Quantity
1


Spark
2


Description
^Jump^Jump 1-2<br/><img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-w58cgFT/0/714fb21b/O/Foulborn_ShieldBash%20%282%29.png" width="400" height="400"><br/>^PushLine^Push 1-2 in a Line<br/>Gain ^Pin^Pin


Trigger



TriggerAction



Print
YES


Public
YES


id
fou23


Power
1


Speed
1


Tactics
0






