Title
Poison Tip


Class
Dreya


Level
S


Scale
0


Image
https://wiki.melvoridle.com/images/thumb/3/3d/Poison_Arrows_%28item%29.png/250px-Poison_Arrows_%28item%29.png


Quantity
1


Spark
1


Description
^Draw^Draw 1<br/>The ^Next^Next card you play will swap its final ^d^ Damage values for ^p^ Poison values


Trigger



TriggerAction



Print
YES


Public
YES


id
dre18


Power
0


Speed
0


Tactics
1






