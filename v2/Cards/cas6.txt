Title
Pickpocket


Class
Cassius


Level
S


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius/i-fF2bc3n/0/021c01c7/O/download%20%2838%29.png


Quantity
1


Spark
0


Description
<img src="https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-dsbtKXh/0/e75b3364/O/Cassius_Pickpocket.png" width="400" height="400"><br/>On ^Hit^Hit, ^Draw^Draw 1<br/><br/>If you are behind the ^TargetOne^Enemy, <br/>^Draw^Draw +1


Trigger



TriggerAction



Print
NO


Public
YES


id
cas6


Power
0


Speed
1


Tactics
0






