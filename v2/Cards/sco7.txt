Title
Heat Metal


Class
Scourge


Level
S


Scale
0


Image
https://thumbs.dreamstime.com/b/molten-metal-poured-ladle-icon-simple-style-isolated-white-background-vector-illustration-91215881.jpg


Quantity
1


Spark
1


Description
Steal up to ^b4^ block from any Unit within ^Range^Range 3<br/><br/>If the Target was an Ally, they Gain any Token of their choice<br/><br/>^PowerOne^= Repeat this on a different Target


Trigger



TriggerAction



Print
YES


Public
YES


id
sco7


Power
1


Speed
0


Tactics
1






