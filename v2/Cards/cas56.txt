Title
Plague Doctor


Class
Cassius


Level
2


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-ZWBwznD/0/8f142257/O/Plague%20Doctor.png


Quantity
1


Spark
2


Description
(^Range^3): Activate the ^p^Poison on an ^TargetOne^Enemy<br/><br/>If they die, Apply their ^p^Poison to the nearest Enemy


Trigger



TriggerAction



Print
NO


Public
NO


id
cas56


Power
0


Speed
1


Tactics
1






