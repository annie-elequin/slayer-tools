Title
MegaMagnet


Class
Tad


Level
2


Type
Card


Scale
0


Image
https://openclipart.org/image/800px/73849


Quantity
1


Spark
1


Description
Select (P) **Unit**s within (2+^spX^) Range and Pull **X** on each **Unit** equal to their Block


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad20


