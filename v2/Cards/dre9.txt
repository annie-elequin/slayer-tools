Title
Forager


Class
Dreya


Level
S


Scale
20


Image
https://cdn-icons-png.flaticon.com/512/621/621717.png


Quantity
1


Spark
1


Description
Look at the top (1+^TacticsX^) Cards in your Draw Pile<br/>^Discard^Discard any of them and <br/>^TopDeck^Topdeck the rest in any order<br/><br/>^TacticsTwo^=Resolve the top card of your Void Pile


Trigger



TriggerAction



Print
YES


Public
YES


id
dre9


Power
1


Speed
0


Tactics
1






