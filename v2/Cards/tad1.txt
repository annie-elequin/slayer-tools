Title
Spark Converter


Class
Tad


Level
1


Type
Power


Scale
0


Image
https://cdn.icon-icons.com/icons2/390/PNG/512/tesla-turret_38799.png


Quantity
1


Spark



Description
^Discard^**Discard 1**. ^Draw^**Draw X** and receive ^dX^ equal to the ^Spark^ on the card **Discarded**


Keep
FALSE


Trigger



TriggerAction



Print
NO


Public
NO


id
tad1


