Title
Bag of Blades


Class
Cassius


Level
2


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-7JxNXjJ/0/fc5eac3e/O/Bag%20of%20Blades.png


Quantity
1


Spark
1


Description



Trigger
Always


TriggerAction
At any time you may place ^Power^ on this card to perform the following:<br/>(^Range^1):^d1^ <br/>If this attack kills, Refund the ^Power^<br/><br/>End-Round: Clear this card if it has 5 or more ^Power^


Print
NO


Public
NO


id
cas50


Power
1


Speed
0


Tactics
0






