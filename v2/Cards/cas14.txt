Title
Trade Routes


Class
Cassius


Level
S


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-rCjFrfB/0/cd91be36/O/download%20%2820%29%20%281%29.png


Quantity
1


Spark
1


Description
Gain 1 of any ^Power^^Speed^^Tactics^Token<br/><br/>All Players may swap up to 2 Tokens for other Token Types


Trigger



TriggerAction



Print
NO


Public
YES


id
cas14


Power
1


Speed
0


Tactics
0






