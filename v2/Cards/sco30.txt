Title
Fire Within


Class
Scourge


Level
1


Type
Card


Scale
0


Image
https://imagedelivery.net/yn0FeX4Pftgdx0G5XU_y6A/scourge_firewithin/public


Quantity
1


Spark
1


Description
^Discard^Discard 0-10<br/><br/>If your HP is equal to the number of cards in your Hand, ^hX^Heal X equal to the total ^Spark^Spark in your hand


Keep
FALSE


Trigger



TriggerAction



Print
YES


Public
YES


id
sco30


