Title
Envenom


Class
Foulborn


Level
2


Scale
20


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-BTbrtqF/0/99a7b754/O/download%20%2810%29.png


Quantity
1


Spark
1


Description
Use this with another card to turn all ^d^Damage values into ^p^Poison values<br/><br/>The card must be at most a:<br/>^spOne^=1-Cost Card<br/>^spTwo^=2-Cost Card<br/>^spThree^=3-Cost or X-Cost Card


Trigger



TriggerAction



Print
NO


Public
NO


id
fou22


Power



Speed



Tactics







