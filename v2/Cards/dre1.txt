Title
The Right Arrow


Class
Dreya


Level
S


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-bKmfTc3/0/e75cdf45/O/download%20%2816%29.png


Quantity
1


Spark
1


Description
^Discard^Discard 1-2 and ^Draw^Draw that many<br/><br/>For each 'Arrow' card you Discarded, ^Draw^Draw +1


Trigger



TriggerAction



Print
YES


Public
YES


id
dre1


Power
1


Speed
0


Tactics
1






