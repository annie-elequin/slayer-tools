Title
Ransom Note


Class
Cassius


Level
1


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-kMcxqCC/0/d41065a2/O/Ransom%20Note.png


Quantity
1


Spark
1


Description
If an adjacent enemy has 1 HP, Choose between:<br/>Gain any 3 Tokens, or<br/>Gain 4 of the same Token type


Trigger



TriggerAction



Print
NO


Public
YES


id
cas45


Power
0


Speed
0


Tactics
1YES



YES


