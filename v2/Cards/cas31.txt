Title
Ominous Onslaught


Class
Cassius


Level
1


Scale
15


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Cassius-May-24-Drop/i-xTF868v/0/4376eb86/O/Ominous%20Onslaught.png


Quantity
1


Spark
0


Description
Deal (0+^PowerX^) ^d^DMG to the closest <br/>^TacticsX^ Enemies ^SpeedX^times


Trigger



TriggerAction



Print
NO


Public
YES


id
cas31


Power
0


Speed
1


Tactics
0






