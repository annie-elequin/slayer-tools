Title
Charbecue


Class
Scourge


Level
S


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Icons/i-vhN4jkx/0/c1beff35/O/Scourge_Charbecue%20%281%29.png


Quantity
1


Spark
2


Description
For each Ally or Enemy targeted, choose ^d1^ or ^h2^Heal 2<br/><br/>^hX^ equal to the total Healing you gave to others


Trigger



TriggerAction



Print
YES


Public
YES


id
sco4


Power
2


Speed
0


Tactics
1






