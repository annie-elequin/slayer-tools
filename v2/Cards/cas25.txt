Title
Distraction


Class
Cassius


Level
1


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Card-Images/Apr-26-Photo-Flurry-IFC/i-WCQHfqZ/0/ccc68000/O/11017253.png


Quantity
1


Spark
1


Description
Choose an Ally or Obstacle within ^Range^Range 3 to ^Taunt^Taunt all Enemies within (1+^TacticsTwo^) spaces of it<br/><br/>Each time an Enemy attacks an Obstacle, ^Wound^Wound them


Trigger



TriggerAction



Print
NO


Public
YES


id
cas25


Power
0


Speed
1


Tactics
0






