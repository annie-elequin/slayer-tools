Title
Anchor Arrow


Class
Dreya


Level
X


Scale
0


Image
https://photos.smugmug.com/Interstellar-Fight-Club-IFC/Dreya/i-XfvSHGc/0/dfce0354/O/Anchor%20Arrow.png


Quantity
1


Spark
2


Description
Mark your ^Initiative^Initiative (before playing this card) with a Token. <br/> Gain ^b4^ and ^Pin^**Pin** yourself for the Round


Trigger
Reaction


TriggerAction
(^RangeLob^2/2): ^dX^ equal to the total ^Initiative^ that has passed since you played this card


Print
NO


Public
NO


id
dre15


Power



Speed
0


Tactics
0






